const Course = require("../models/Course");
const User = require('../models/User');



// Creating a new course
module.exports.addCourse = (reqBody) => {

	
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});


	// Saves the created object to our database
	return newCourse.save().then((course, error) => {

		// Course creation failed
		if(error) {

			return false

		// Course creation successful
		} else {

			return true
		}
	})
}




// Retriving All Courses
module.exports.getAllCourses = (data) => {

	if(data.isAdmin) {
		return Course.find({}).then(result => {

			return result
		})
	} else {
		return false // "You are not an Admin."
	}
};


// Retrieve All Active Courses
module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {

		return result
	})
};


// Retrieve a Specific Course
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {

		return result
	})
};


// Update a Specific Course
module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {

		console.log(updatedCourse)
		if(error) {
			return false
		} else {
			return true
		}
	}) 

};




module.exports.archiveCourse = (data) => {

	return Course.findById(data.courseId).then((result, err) => {

		if(data.isAdmin === true) {

			result.isActive = false;

			return result.save().then((archivedCourse, err) => {

				// Course not archived
				if(err) {

					return false;

				// Course archived successfully
				} else {

					return true;
				}
			})

		} else {

			//If user is not Admin
			return false
		}

	})
};



module.exports.activeCourse = (data) => {

	return Course.findById(data.courseId).then((result, err) => {

		if(data.isAdmin === true) {

			result.isActive = true;

			return result.save().then((activeCourse, err) => {

				// Course not activated
				if(err) {

					return false;

				// Course activated successfully
				} else {

					return true;
				}
			})

		} else {

			//If user is not Admin
			return false
		}

	})
};
